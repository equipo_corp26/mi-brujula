@extends('layouts.auth')

@section('content')
<form method="POST" action="{{ route('password.update') }}">
    @csrf
    {{-- token --}}
    <input type="hidden" name="token" value="{{ $token }}">
    {{-- correo --}}
    <div class="">
        <label class="">Correo</label>
        <input type="email" name="email" required autofocus value="{{ $email ?? old('email') }}">
        @error('email')
            <span class="">{{$message}}</span>
        @enderror
    </div>
    {{-- contraseña --}}
    <div class="">
        <label class="">Contraseña</label>
        <input type="password" name="password" required>
        @error('password')
            <span class="">{{$message}}</span>
        @enderror
    </div>
    {{-- contraseña --}}
    <div class="">
        <label class="">Repetir contraseña</label>
        <input type="password" name="password_confirmation" required>
        @error('password_confirmation')
            <span class="">{{$message}}</span>
        @enderror
    </div>
    {{-- enviar --}}
    <div class="">
        <button>Restablecer contraseña</button>
    </div>
</form>
@endsection
