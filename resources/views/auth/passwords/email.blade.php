@extends('layouts.auth')

@section('content')
{{-- notificacion envio de correo --}}
@if (session('status'))
    <div class="" role="alert">
        {{ session('status') }}
    </div>
@endif
<form method="POST" action="{{ route('password.email') }}">
    @csrf
    {{-- correo --}}
    <div class="">
        <label class="">Correo</label>
        <input type="email" name="email" required autofocus>
        @error('email')
            <span class="">{{$message}}</span>
        @enderror
    </div>
    {{-- enviar --}}
    <div class="">
        <button>Enviar correo de recuperacion</button>
    </div>
</form>
@endsection
