@extends('layouts.auth')

@section('content')
    <form action="{{ route('login') }}" method="post">
        @csrf
        {{-- correo --}}
        <div class="">
            <label class="">Correo</label>
            <input type="email" name="email" required>
            @error('email')
                <span class="">{{$message}}</span>
            @enderror
        </div>
        {{-- contraseña --}}
        <div class="">
            <label class="">Contraseña</label>
            <input type="password" name="password" required>
            @error('password')
                <span class="">{{$message}}</span>
            @enderror
        </div>
        {{-- recuerdame --}}
        <div class="">
            <input type="checkbox" name="remember" required> Recuerdame
        </div>
        {{-- enviar --}}
        <div class="">
            <button>Ingresar</button>
        </div>
        {{-- olvide mi contraseña --}}
        @if (Route::has('password.request'))
            <a class="" href="{{ route('password.request') }}">
                {{ __('Olvide mi contraseña') }}
            </a>
        @endif
    </form>
@endsection
