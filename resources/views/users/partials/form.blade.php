<div class="col-md-6 my-2 form-group">
    <label>Nombre de usuario</label>
    <input class="form-control" @isset($user) value="{{$user->name}}" @endisset type="text" required name="name">
    @error('name')
        {{$message}}
    @enderror
</div>
@isset($user)    
    <div class="col-md-6 my-2 form-group">
        <label>Correo</label>
        <p class="form-control bg-secondary text-light">{{$user->email}}</p>
    </div>
@else
    <div class="col-md-6 my-2 form-group">
        <label>Correo</label>
        <input class="form-control" type="text" required name="email">
        @error('email')
            {{$message}}
        @enderror
    </div>
@endisset
<div class="col-md-6 my-2 form-group">
    <label>Rol</label>
    <select class="form-control text-capitalize" required name="role">
        @foreach ($roles as $role)
            <option class="text-capitalize" @isset($user){{$user->hasRole($role->name) ? 'selected' : ''}}@endisset value="{{$role->name}}">{{$role->name}}</option>
        @endforeach
    </select>
    @error('role')
        {{$message}}
    @enderror
</div>

<div class="form-group col-12 my-2">
    <button class="btn btn-outline-danger" type="reset">Limpiar</button>
    <button class="btn btn-primary float-right" type="submit">Aceptar</button>
</div>