@extends('layouts.app')

@section('icon')
    <i class="pe-7s-user icon-gradient bg-sunny-morning"></i>
@endsection

@section('title')
    Registrar usuario
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('users.index') }}">Lista de usuarios</a></li>
    <li class="active breadcrumb-item" aria-current="page">Registrar usuario</li>
@endsection

@section('button_title')
    <a href="{{ route('users.index') }}" type="button" data-toggle="tooltip" title="" data-placement="bottom" class="btn-shadow mr-3 btn btn-primary" data-original-title="Regresar" aria-describedby="tooltip109285">
        <i class="fa fa-arrow-left"></i>
    </a>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form class="form-row" action="{{ route('users.store') }}" method="POST">
                    @csrf
                    @include('users.partials.form')
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
