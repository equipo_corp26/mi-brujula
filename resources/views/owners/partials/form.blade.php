<div class="col-md-6 my-2 form-group">
    <label>Nombre del dueño</label>
    <input class="form-control" @isset($owner) value="{{$owner->name}}" @endisset type="text" required name="name">
    @error('name')
        {{$message}}
    @enderror
</div>

<div class="form-group col-12 my-2">
    <button class="btn btn-outline-danger" type="reset">Limpiar</button>
    <button class="btn btn-primary float-right" type="submit">Aceptar</button>
</div>