@extends('layouts.app')

@section('icon')
    <i class="pe-7s-users icon-gradient bg-sunny-morning"></i>
@endsection

@section('title')
    Lista de dueños
@endsection

@section('button_title')
    <a href="{{ route('owners.create') }}" type="button" data-toggle="tooltip" title="" data-placement="bottom" class="btn-shadow mr-3 btn btn-primary" data-original-title="Agregar" aria-describedby="tooltip109285">
        <i class="fa fa-plus"></i>
    </a>
@endsection

@section('breadcrumb')
    <li class="active breadcrumb-item" aria-current="page">Lista de dueños</li>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table" id="table-index">
                        <thead class="text-light bg-primary">
                            <tr>
                                <th>Nombre</th>
                                <th>Creado</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($owners as $item)
                                <tr>
                                    <td>{{$item->name}}</td>
                                    <td>{{$item->created_at->format('d-m-Y')}}</td>
                                    <td><a data-toggle="tooltip" title="Editar" class="btn btn-sm btn-warning" href="{{ route('owners.edit', $item) }}"><i class="fa fa-edit"></i></a></td>
                                    <td><button data-toggle="tooltip" title="Eliminar" class="btn btn-sm btn-outline-danger" onclick="deleteItem('{{$item->slug}}')"><i class="fa fa-trash"></i></button></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <form action="" method="post" id="form-delete-item">
        @csrf @method('DELETE')
    </form>
</div>
@endsection

@section('scripts')
    <script src="{{ asset('js/jquery-3.5.1.js') }}"></script>
    <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/dataTables.bootstrap4.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#table-index').DataTable();
        } );
    </script>
    <script>
        function deleteItem(item)
        {
            swal({
                    title: "¿Eliminar registro?",
                    text: "¿Esta seguro que desea eliminar este registro?",
                    icon: "warning",
                    buttons: ['Cancelar','Eliminar'],
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        document.querySelector("#form-delete-item").action = window.location.origin + '/owners/' + item
                        document.querySelector("#form-delete-item").submit()
                    }
                })
        }
    </script>
@endsection

@section('styles')
    <link href="{{asset('css/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
@endsection