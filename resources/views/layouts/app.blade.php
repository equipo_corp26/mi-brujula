<!doctype html>
<html lang="en">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="es">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <meta name="msapplication-tap-highlight" content="no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Mi Brujula - @yield('title','Bienvenido')</title>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <!-- Styles -->
    <link href="{{asset('theme/main.css')}}" rel="stylesheet">
    @yield('styles')
    <style>
        .img-errors{
            height: 250px;
            background-size: contain;
            background-position: center;
            background-repeat: no-repeat
        }
    </style>
</head>
<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header" id="app">
        @include('commons.navbar')

        <div class="app-main">
            @include('commons.sidebar')
            <div class="app-main__outer">
                <div class="app-main__inner">
                    <div class="app-page-title">
                        <div class="page-title-wrapper">
                            <div class="page-title-heading">
                                <div class="page-title-icon">
                                    @yield('icon')
                                </div>
                                <div>
                                    @yield('title')
                                    <div class="page-title-subheading">
                                        <nav class="" aria-label="breadcrumb">
                                            <ol class="breadcrumb">
                                                <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">Panel Principal</a></li>
                                                @yield('breadcrumb')
                                            </ol>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                            <div class="page-title-actions">
                                @yield('button_title')
                            </div>
                        </div>
                    </div>
                    @if (session('message'))
                        <div class="alert alert-info fade show" role="alert">
                            <p class="mb-0">{{session('message')}}</p>
                        </div>
                    @endif
                    @yield('content')
                </div>
                @include('commons.footer')
            </div>
        </div>
    </div>
    <!-- Scripts -->
    <script src="{{ asset('js/sweetalert.min.js') }}"></script>
    <script src="{{ asset('theme/assets/scripts/main.js') }}"></script>
    @yield('scripts')
</body>
</html>
