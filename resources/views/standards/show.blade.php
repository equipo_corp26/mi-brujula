@extends('layouts.app')

@section('icon')
    <i class="pe-7s-star icon-gradient bg-sunny-morning"></i>
@endsection

@section('title')
    Detalles del criterio
@endsection

@section('button_title')
    <a href="{{ route('standards.index') }}" type="button" data-toggle="tooltip" title="" data-placement="bottom" class="btn-shadow mr-3 btn btn-primary" data-original-title="Regresar" aria-describedby="tooltip109285">
        <i class="fa fa-arrow-left"></i>
    </a>
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('standards.index') }}">Lista de criterios</a></li>
    <li class="active breadcrumb-item" aria-current="page">Detalles del criterio</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card my-3">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 my-2">
                            <h3 class="text-capitalize">{{$standard->name}}</h3>
                        </div>
                        <div class="col-12 my-2 text-justify">
                            <p>{{$standard->description}}</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card my-2">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <h4>Agregar aspecto</h4>
                        </div>
                        <div class="col-12">
                            <form class="form-row" method="POST" action="{{ route('aspects.store') }}">
                                @csrf
                                <input type="hidden" name="standard_id" value="{{$standard->slug}}">
                                <div class="form-group col-lg-4">
                                    <label>Aspecto</label>
                                    <textarea name="description" required rows="3" class="form-control"></textarea>
                                </div>
                                <div class="form-group col-lg-3">
                                    <label>Calificación 1</label>
                                    <textarea name="min_rating" rows="3" class="form-control"></textarea>
                                </div>
                                <div class="form-group col-lg-3">
                                    <label>Calificación 5</label>
                                    <textarea name="max_rating" rows="3" class="form-control"></textarea>
                                </div>
                                <div class="form-group col-lg-1 text-center">
                                    <label>Limpiar</label>
                                    <button type="reset" class="btn btn-outline-danger"><i class="fa fa-times"></i></button>
                                </div>
                                <div class="form-group col-lg-1 text-center">
                                    <label>Agregar</label>
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card my-2">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <h4>({{$standard->aspects->count()}}) Aspectos</h4>
                        </div>
                        <div class="col-12">
                            @foreach ($standard->aspects as $aspect)    
                                <form method="POST" action="{{ route('aspects.update',$aspect) }}" class="form-row border-bottom border-primary p-2">
                                    @method('PUT')
                                    @csrf
                                    <div class="form-group col-lg-4">
                                        <label>Aspecto</label>
                                        <textarea name="description" required rows="3" class="form-control">{{$aspect->description}}</textarea>
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <label>Calificación 1</label>
                                        <textarea name="min_rating" rows="3" class="form-control">{{$aspect->min_rating}}</textarea>
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <label>Calificación 5</label>
                                        <textarea name="max_rating" rows="3" class="form-control">{{$aspect->max_rating}}</textarea>
                                    </div>
                                    <div class="form-group col-lg-1 text-center">
                                        <label>Actualizar</label>
                                        <button type="submit" class="btn btn-warning"><i class="fa fa-edit"></i></button>
                                    </div>
                                    <div class="form-group col-lg-1 text-center">
                                        <label>Eliminar</label>
                                        <button onclick="deleteItem('{{$aspect->id}}')" type="submit" class="btn btn-outline-danger"><i class="fa fa-trash"></i></button>
                                    </div>
                                </form>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <form action="" method="post" id="form-delete-item">
        @csrf @method('DELETE')
    </form>
@endsection

@section('scripts')
    <script>
        function deleteItem(item)
        {
            swal({
                    title: "¿Eliminar registro?",
                    text: "¿Esta seguro que desea eliminar este registro?",
                    icon: "warning",
                    buttons: ['Cancelar','Eliminar'],
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        document.querySelector("#form-delete-item").action = window.location.origin + '/aspects/' + item
                        document.querySelector("#form-delete-item").submit()
                    }
                })
        }
    </script>
@endsection
