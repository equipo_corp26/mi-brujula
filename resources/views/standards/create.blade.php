@extends('layouts.app')

@section('icon')
    <i class="pe-7s-star icon-gradient bg-sunny-morning"></i>
@endsection

@section('title')
    Registrar criterio
@endsection

@section('button_title')
    <a href="{{ route('standards.index') }}" type="button" data-toggle="tooltip" title="" data-placement="bottom" class="btn-shadow mr-3 btn btn-primary" data-original-title="Regresar" aria-describedby="tooltip109285">
        <i class="fa fa-arrow-left"></i>
    </a>
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('standards.index') }}">Lista de criterios</a></li>
    <li class="active breadcrumb-item" aria-current="page">Registrar criterio</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form class="form-row" action="{{ route('standards.store') }}" method="POST">
                        @csrf
                        @include('standards.partials.form')
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
