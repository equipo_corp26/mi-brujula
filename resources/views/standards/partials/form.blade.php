<div class="col-12 my-2 form-group">
    <label>Nombre del criterio</label>
    <input class="form-control" @isset($standard) value="{{$standard->name}}" @endisset type="text" required name="name">
    @error('name')
        {{$message}}
    @enderror
</div>
<div class="col-12 my-2 form-group">
    <label>Descripción</label>
    <textarea required class="form-control" name="description" rows="4">@isset($standard){{$standard->description}}@endisset</textarea>
    @error('description')
        {{$message}}
    @enderror
</div>

<div class="form-group col-12 my-2">
    <button class="btn btn-outline-danger" type="reset">Limpiar</button>
    <button class="btn btn-primary float-right" type="submit">Aceptar</button>
</div>