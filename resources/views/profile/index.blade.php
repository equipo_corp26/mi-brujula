@extends('layouts.app')

@section('content')
    {{-- avatar --}}
    <div>
        <div>
            <img id="picture" width="100px" src="{{ Auth::user()->avatar ? Storage::url(Auth::user()->avatar) : asset('img/avatar.png') }}" alt="avatar">
        </div>
        <div>
            <form action="{{ route('profile.avatar', ['id'=>1]) }}" enctype="multipart/form-data" method="POST">
                @csrf
                <label>Cambiar imagen</label>
                <input id="file-upload" type="file" name="avatar" required>
                <button type="submit">Actualizar</button>
            </form>
        </div>
    </div>
    {{-- datos del usuario --}}
    <div>
        <div>
            <h4>Datos del usuario</h4>
        </div>
        <div>
            <p>Nombre de usuario: {{$user->name}}</p>
        </div>
        <div>
            <p>Correo electrónico: {{$user->email}}</p>
        </div>
    </div>
    {{-- cambiar contraseña --}}
    <div>
        <div>
            <h4>Actualizar contraseña</h4>
        </div>
        <form method="POST" action="{{ route('profile.password') }}">
            @method('PUT')
            @csrf
            <div>
                <label>Nueva contraseña</label>
                <input type="password" name="password" required min="8">
                @error('password')
                    {{$message}}
                @enderror
            </div>
            <div>
                <label>Repetir contraseña</label>
                <input type="password" name="password_confirmation" required>
                @error('password_confirmation')
                    {{$message}}
                @enderror
            </div>
            <button class="submit">Actualizar contraseña</button>
        </form>
    </div>
@endsection


@section('scripts')
    <script>
        document.getElementById("file-upload").addEventListener('change', cambiarImagen);
        function cambiarImagen(event){
            var file = event.target.files[0];

            var reader = new FileReader();
            reader.onload = (event) => {
                document.getElementById("picture").setAttribute('src', event.target.result);
            };

            reader.readAsDataURL(file);
        }
    </script>
@endsection