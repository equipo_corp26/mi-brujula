@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-lg-10 my-3 text-center">
            <h1>Error 403</h1>
            <p>No tienes permiso para realizar esta acción</p>
        </div>
        <div class="col-12 col-md-8 col-lg-6 img-errors text-center" style="background-image: url({{ asset('img/errors/403.png') }})"></div>
        <div class="col-12 my-3 text-center">
            <a href="{{route('dashboard.index') }}" class="btn btn-primary">Regresar a la página principal</a>
        </div>
    </div>
@endsection