@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-lg-10 my-3 text-center">
            <h1>Error 404</h1>
            <p>Esta URL no se ecnuentra registrada en nuestro sistema</p>
        </div>
        <div class="col-12 col-md-8 col-lg-6 img-errors text-center" style="background-image: url({{ asset('img/errors/404.png') }})"></div>
        <div class="col-12 text-center my-3">
            <a href="{{route('dashboard.index') }}" class="btn btn-primary">Regresar a la página principal</a>
        </div>
    </div>
@endsection