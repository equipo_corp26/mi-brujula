<div class="app-sidebar sidebar-shadow  sidebar-text-light" style="background-color: #faa635">
    <div class="app-header__logo">
        <div class="logo-src"></div>
        <div class="header__pane ml-auto">
            <div>
                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
    <div class="app-header__mobile-menu">
        <div>
            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
        </div>
    </div>
    <div class="app-header__menu">
        <span>
            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                <span class="btn-icon-wrapper">
                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                </span>
            </button>
        </span>
    </div>    
    <div class="scrollbar-sidebar ps ps--active-y">
        <div class="app-sidebar__inner">
            <ul class="vertical-nav-menu metismenu">
                <li class="app-sidebar__heading">Panel Principal</li>
                <li>
                    <a href="{{ route('dashboard.index') }}">
                        <i class="metismenu-icon pe-7s-monitor"></i>
                        Panel Principal
                    </a>
                </li>
                <li class="app-sidebar__heading">Administración</li>
                {{-- Usuarios --}}
                <li>
                    <a href="#">
                        <i class="metismenu-icon pe-7s-users"></i>
                        Usuarios
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul class="mm-collapse">
                        <li>
                            <a href="{{ route('users.index') }}"> <i class="pe-7s-note2"></i> Lista de usuarios </a>
                        </li>
                        <li>
                            <a href="{{ route('users.create') }}"> <i class="pe-7s-plus"></i> Registrar usuario </a>
                        </li>
                    </ul>
                </li>
                <li class="app-sidebar__heading">Programas</li>
                {{-- Dueños --}}
                <li>
                    <a href="#">
                        <i class="metismenu-icon pe-7s-id"></i>
                        Dueños
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul class="mm-collapse">
                        <li>
                            <a href="{{ route('owners.index') }}"> <i class="pe-7s-note2"></i> Lista de dueños </a>
                        </li>
                        <li>
                            <a href="{{ route('owners.create') }}"> <i class="pe-7s-plus"></i> Registrar dueño </a>
                        </li>
                    </ul>
                </li>
                {{-- Dueños --}}
                <li>
                    <a href="#">
                        <i class="metismenu-icon pe-7s-star"></i>
                        Criterios
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul class="mm-collapse">
                        <li>
                            <a href="{{ route('standards.index') }}"> <i class="pe-7s-note2"></i> Lista de criterios </a>
                        </li>
                        <li>
                            <a href="{{ route('standards.create') }}"> <i class="pe-7s-plus"></i> Registrar criterio </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; height: 640px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 605px;"></div></div></div>
</div>