<?php

namespace Database\Factories\Owner;

use App\Models\Owner\Owner;
use Illuminate\Database\Eloquent\Factories\Factory;

class OwnerFactory extends Factory
{
    protected $model = Owner::class;

    public function definition()
    {
        return [
            'name' => $this->faker->company(),
        ];
    }
}
