<?php

namespace Database\Factories\Aspect;

use App\Models\Aspect\Aspect;
use Illuminate\Database\Eloquent\Factories\Factory;

class AspectFactory extends Factory
{
    protected $model = Aspect::class;

    public function definition()
    {
        return [
            'description' => $this->faker->text(250),
            'min_rating' => $this->faker->text(250),
            'max_rating' => $this->faker->text(250),
        ];
    }
}
