<?php

namespace Database\Factories\Standard;

use App\Models\Standard\Standard;
use Illuminate\Database\Eloquent\Factories\Factory;

class StandardFactory extends Factory
{
    protected $model = Standard::class;

    public function definition()
    {
        return [
            'name' => $this->faker->sentence(3),
            'description' => $this->faker->text(350),
        ];
    }
}
