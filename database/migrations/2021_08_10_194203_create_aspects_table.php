<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAspectsTable extends Migration
{
    public function up()
    {
        Schema::create('aspects', function (Blueprint $table) {
            $table->id();
            $table->foreignId('standard_id')->constrained('standards','id');
            $table->text('description');
            $table->text('max_rating');
            $table->text('min_rating');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aspects');
    }
}
