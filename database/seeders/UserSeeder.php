<?php

namespace Database\Seeders;

use App\Models\User\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    public function run()
    {
        foreach(User::all() as $user)
        {
            $user->assignRole('evaluador');
        }
        User::create([
            'name'               => 'angel',
            'email'              => 'angel@gmail.com',
            'password'           => 26153290,
            'email_verified_at'  => now(),
        ])->assignRole('super-admin');

    }    
}
