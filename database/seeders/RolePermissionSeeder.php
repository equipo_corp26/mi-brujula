<?php

namespace Database\Seeders;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Database\Seeder;

class RolePermissionSeeder extends Seeder
{
    public function run()
    {
        /* Roles */
        Role::create(['name' => 'super-admin']);
        $admin = Role::create(['name' => 'administrador']);
        $basic = Role::create(['name' => 'evaluador']);
        /* Permisos */
            /* Usuarios */
            Permission::create(['name' => 'users.index']);
            Permission::create(['name' => 'users.create']);
            Permission::create(['name' => 'users.update']);
            Permission::create(['name' => 'users.destroy']);
            /* Dueños */
            Permission::create(['name' => 'owners.index']);
            Permission::create(['name' => 'owners.create']);
            Permission::create(['name' => 'owners.update']);
            Permission::create(['name' => 'owners.destroy']);
            /* Criterios */
            Permission::create(['name' => 'standards.index']);
            Permission::create(['name' => 'standards.create']);
            Permission::create(['name' => 'standards.update']);
            Permission::create(['name' => 'standards.destroy']);
        /* Asignar Permisos al rol */
        $admin->givePermissionTo( Permission::all()->pluck('name') );
    }
}
