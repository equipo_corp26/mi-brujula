<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        /* Roles y permisos */   
        $this->call(RolePermissionSeeder::class);
        /* Usuarios */
        \App\Models\User\User::factory(20)->create();
        $this->call(UserSeeder::class);
        /* Dueños */
        \App\Models\Owner\Owner::factory(20)->create();
        /* Criterios */
        \App\Models\Standard\Standard::factory(15)
                                        ->has( \App\Models\Aspect\Aspect::factory()->count(4) )
                                        ->create();
    }
}
