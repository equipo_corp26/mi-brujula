<?php

namespace App\Mail\User;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RegisterUser extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    public $password;

    public function __construct($data,$password)
    {
        $this->data = $data;
        $this->password = $password;
    }

    public function build()
    {
        return $this->from( $this->data['email'] )
                    ->subject('Creación de usuario')
                    ->view('emails.users.register');
    }
}
