<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PasswordChanged
{
    public function handle(Request $request, Closure $next)
    {
        if(Auth::check())
        {
            if(!Auth::user()->password_changed )
            {
                return redirect()->route('profile.index')->with('message','Por favor cambie su contraseña para poder utilizar el sistema');
            }
        }
        return $next($request);
    }
}
