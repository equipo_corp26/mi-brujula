<?php

namespace App\Http\Controllers;
/* modelos */
use App\Models\User\User;
/* request */
use App\Http\Requests\User\UserStoreRequest;
use App\Http\Requests\User\UserUpdateRequest;
/* correos */
use App\Mail\User\RegisterUser;
use Illuminate\Support\Facades\Mail;
/* paquetes */
use Spatie\Permission\Models\Role;
/* Illuminate */
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /* Constructor */
    public function __construct()
    {
        $this->middleware('can:users.index')->only('index');
        $this->middleware('can:users.create')->only(['create','store']);
        $this->middleware('can:users.update')->only(['edit','update']);
        $this->middleware('can:users.destroy')->only('destroy');
    }
    /* Crud */
    public function index()
    {
        $users = User::where('id','!=',Auth::id())->get();
        return view('users.index',compact('users'));
    }
    public function create()
    {
        $roles = Role::where('name','!=','super-admin')->orderBy('name','ASC')->get();
        return view('users.create',compact('roles'));
    }
    public function store(UserStoreRequest $request)
    {
        /* Crear usuario */
        $user = User::make( $request->all() );
        /* Contraseña */
        $password = substr(str_shuffle("0123456789abcdefghijklmnñopqrstvwxyz+-@"), 0, 12);
        $user->password = $password;
        $user->save();
        /* Asignar rol */
        $user->assignRole($request->role);
        /* Email */
        $mail = new RegisterUser($user,$password);
        Mail::to('angel@gmail.com')->send($mail);
        /* Return */
        return redirect()->route('users.index')->with(['message' => 'Registro almacenado con éxito']);
    }
    public function edit(User $user)
    {
        $roles = Role::where('name','!=','super-admin')->orderBy('name','ASC')->get();
        return view('users.edit',compact('roles','user'));
    }
    public function update(UserUpdateRequest $request, User $user)
    {
        $user->update( $request->all() );
        $user->syncRoles($request->role);
        return redirect()->route('users.index')->with(['message' => 'Registro actualizado con éxito']);
    }
    public function destroy(User $user)
    {
        $user->delete();
        return redirect()->route('users.index')->with(['message' => 'Registro eliminado con éxito']);
    }
}
