<?php

namespace App\Http\Controllers;

use App\Http\Requests\Aspect\AspectStoreRequest;
use App\Http\Requests\Aspect\AspectUpdateRequest;
use App\Models\Aspect\Aspect;
use App\Models\Standard\Standard;
use Illuminate\Http\Request;

class AspectController extends Controller
{
    /* Constructor */
    public function __construct()
    {
        $this->middleware('can:aspects.create')->only(['store']);
        $this->middleware('can:aspects.update')->only(['update']);
        $this->middleware('can:aspects.destroy')->only('destroy');
    }
    /* Crud */
    public function store(AspectStoreRequest $request)
    {
        $standard = Standard::where('slug',$request->standard_id)->first();
        $standard->aspects()->create($request->except('standard_id'));
        /* Return */
        return redirect()->back()->with(['message' => 'Registro almacenado con éxito']);
    }
    public function update(AspectUpdateRequest $request, Aspect $aspect)
    {
        $aspect->update( $request->all() );
        return redirect()->back()->with(['message' => 'Registro actualizado con éxito']);
    }
    public function destroy(Aspect $aspect)
    {
        $aspect->delete();
        return redirect()->back()->with(['message' => 'Registro eliminado con éxito']);
    }
}
