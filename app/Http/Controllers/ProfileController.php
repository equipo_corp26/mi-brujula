<?php

namespace App\Http\Controllers;


/* modelos */
use App\Models\User\User;
/* request */
use App\Http\Requests\Profile\AvatarRequest;
use App\Http\Requests\Profile\ChangePasswordRequest;
/* illuminate */
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ProfileController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        return view('profile.index',compact('user'));
    }
    public function avatar(AvatarRequest $request)
    {
        if ( Auth::user()->avatar ) {
            Storage::delete( Auth::user()->avatar );
        }
        $user = User::find( Auth::id() )->update([
            'avatar' => $request->file('avatar')->store('avatars')
        ]);
        /* Return */
        return redirect()->route('profile.index')->with(['message' => 'Avatar actualizado con éxito']);
    }
    public function changePassword(ChangePasswordRequest $request)
    {
        $user = User::find( Auth::id() )->update([
            'password'         => $request->password,
            'password_changed' => 1
        ]);
        /* Return */
        return redirect()->route('profile.index')->with(['message' => 'Contraseña actualizada con éxito']);
    }
}
