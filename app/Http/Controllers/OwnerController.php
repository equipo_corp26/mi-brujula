<?php

namespace App\Http\Controllers;

/* modelos */
use App\Models\Owner\Owner;
/* request */
use App\Http\Requests\Owner\OwnerStoreRequest;
use App\Http\Requests\Owner\OwnerUpdateRequest;
/* paquetes */
use Spatie\Permission\Models\Role;

class OwnerController extends Controller
{
    /* Constructor */
    public function __construct()
    {
        $this->middleware('can:owners.index')->only('index');
        $this->middleware('can:owners.create')->only(['create','store']);
        $this->middleware('can:owners.update')->only(['edit','update']);
        $this->middleware('can:owners.destroy')->only('destroy');
    }
    /* Crud */
    public function index()
    {
        $owners = Owner::get();
        return view('owners.index',compact('owners'));
    }
    public function create()
    {
        return view('owners.create');
    }
    public function store(OwnerStoreRequest $request)
    {
        $owner = Owner::create( $request->all() );
        /* Return */
        return redirect()->route('owners.index')->with(['message' => 'Registro almacenado con éxito']);
    }
    public function edit(Owner $owner)
    {
        return view('owners.edit',compact('owner'));
    }
    public function update(OwnerUpdateRequest $request, Owner $owner)
    {
        $owner->update( $request->all() );
        return redirect()->route('owners.index')->with(['message' => 'Registro actualizado con éxito']);
    }
    public function destroy(Owner $owner)
    {
        $owner->delete();
        return redirect()->route('owners.index')->with(['message' => 'Registro eliminado con éxito']);
    }
}
