<?php

namespace App\Http\Controllers;

/* modelos */
use App\Models\Standard\Standard;
/* request */
use App\Http\Requests\Standard\StandardStoreRequest;
use App\Http\Requests\Standard\StandardUpdateRequest;
/* paquetes */
use Spatie\Permission\Models\Role;

class StandardController extends Controller
{
    /* Constructor */
    public function __construct()
    {
        $this->middleware('can:standards.index')->only('index');
        $this->middleware('can:standards.create')->only(['create','store']);
        $this->middleware('can:standards.update')->only(['edit','update']);
        $this->middleware('can:standards.destroy')->only('destroy');
    }
    /* Crud */
    public function index()
    {
        $standards = Standard::withCount('aspects')->get();
        return view('standards.index',compact('standards'));
    }
    public function create()
    {
        $roles = Role::where('name','!=','super-admin')->orderBy('name','ASC')->get();
        return view('standards.create',compact('roles'));
    }
    public function store(StandardStoreRequest $request)
    {
        $standard = Standard::create( $request->all() );
        /* Return */
        return redirect()->route('standards.index')->with(['message' => 'Registro almacenado con éxito']);
    }
    public function show(Standard $standard)
    {
        $standard->load('aspects');
        return view('standards.show',compact('standard'));
    }
    public function edit(Standard $standard)
    {
        return view('standards.edit',compact('standard'));
    }
    public function update(StandardUpdateRequest $request, Standard $standard)
    {
        $standard->update( $request->all() );
        return redirect()->route('standards.index')->with(['message' => 'Registro actualizado con éxito']);
    }
    public function destroy(Standard $standard)
    {
        $standard->delete();
        return redirect()->route('standards.index')->with(['message' => 'Registro eliminado con éxito']);
    }
}
