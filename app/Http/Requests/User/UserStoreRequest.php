<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UserStoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'  => 'required|min:3',
            'email' => 'required|email|unique:users,email',
            'role'  => 'required|exists:roles,name'
        ];
    }
}
