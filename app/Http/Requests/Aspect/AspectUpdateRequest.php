<?php

namespace App\Http\Requests\Aspect;

use Illuminate\Foundation\Http\FormRequest;

class AspectUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'description' => 'required',
            'min_rating' => 'required',
            'max_rating' => 'required',
        ];
    }
}
