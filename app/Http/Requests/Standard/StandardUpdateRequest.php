<?php

namespace App\Http\Requests\Standard;

use Illuminate\Foundation\Http\FormRequest;

class StandardUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'description' => 'required|min:10',
        ];
    }
}
