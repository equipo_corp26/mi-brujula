<?php

namespace App\Http\Requests\Standard;

use Illuminate\Foundation\Http\FormRequest;

class StandardStoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'description' => 'required|min:10',
        ];
    }
}
