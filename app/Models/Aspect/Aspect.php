<?php

namespace App\Models\Aspect;

/* Traits */
use App\Models\Aspect\Traits\AspectMutators;
use App\Models\Aspect\Traits\AspectRelationship;
/* illuminate */
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Aspect extends Model
{
    use HasFactory,
        AspectMutators,
        AspectRelationship;

    protected $fillable = [
        'max_rating',
        'min_rating',
        'description',
        'standard_id',
    ];
}
