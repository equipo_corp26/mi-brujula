<?php

namespace App\Models\Aspect\Traits;

trait AspectMutators {
    /* setters */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = strtolower($value);
    }
}