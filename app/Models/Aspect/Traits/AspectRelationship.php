<?php

namespace App\Models\Aspect\Traits;

use App\Models\Standard\Standard;

trait AspectRelationship {
    public function standard()
    {
        return $this->hasMany(Standard::class);
    }
}