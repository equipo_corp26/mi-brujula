<?php

namespace App\Models\User;

/* Traits */
use App\Models\User\Traits\UserMutators;
use App\Models\User\Traits\UserRelationship;
/* paquetes */
use Spatie\Permission\Traits\HasRoles;
use Cviebrock\EloquentSluggable\Sluggable;
/* illuminate */
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasFactory,
        Notifiable,
        HasRoles,
        Sluggable,
        UserMutators,
        UserRelationship;

    protected $fillable = [
        'name',
        'slug',
        'email',
        'avatar',
        'password',
        'password_changed',
    ];
    protected $hidden = [
        'password',
        'remember_token',
    ];
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function getRouteKeyName()
    {
        return 'slug';
    }
    public function sluggable(): array
    {
        return ['slug' => ['source' => 'name']];
    }
}
