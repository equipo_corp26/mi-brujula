<?php

namespace App\Models\User\Traits;
use App\Models\User;

trait UserMutators {
    /* setters */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = strtolower($value);
    }
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

}