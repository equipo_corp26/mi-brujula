<?php

namespace App\Models\Standard\Traits;

trait StandardMutators {
    /* setters */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = strtolower($value);
    }
}