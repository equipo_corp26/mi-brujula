<?php

namespace App\Models\Standard\Traits;

use App\Models\Aspect\Aspect;

trait StandardRelationship {
    public function aspects()
    {
        return $this->hasMany(Aspect::class);
    }
}