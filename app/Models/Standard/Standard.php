<?php

namespace App\Models\Standard;

/* Traits */
use App\Models\Standard\Traits\StandardMutators;
use App\Models\Standard\Traits\StandardRelationship;
/* paquetes */
use Cviebrock\EloquentSluggable\Sluggable;
/* illuminate */
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Standard extends Model
{
    use HasFactory,
        Sluggable,
        StandardMutators,
        StandardRelationship;

    protected $fillable = [
        'name',
        'slug',
        'description',
    ];
    public function getRouteKeyName()
    {
        return 'slug';
    }
    public function sluggable(): array
    {
        return ['slug' => ['source' => 'name']];
    }
}
