<?php

namespace App\Models\Owner;

/* Traits */
use App\Models\Owner\Traits\OwnerMutators;
use App\Models\Owner\Traits\OwnerRelationship;
/* paquetes */
use Cviebrock\EloquentSluggable\Sluggable;
/* illuminate */
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Owner extends Model
{
    use HasFactory,
        Sluggable,
        OwnerMutators,
        OwnerRelationship;

    protected $fillable = [
        'name',
        'slug',
    ];
    public function getRouteKeyName()
    {
        return 'slug';
    }
    public function sluggable(): array
    {
        return ['slug' => ['source' => 'name']];
    }
}
