<?php

namespace App\Models\Owner\Traits;

trait OwnerMutators {
    /* setters */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = strtolower($value);
    }
}