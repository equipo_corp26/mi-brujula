<?php

use App\Http\Controllers\AspectController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\OwnerController;
use App\Http\Controllers\StandardController;
use Illuminate\Support\Facades\Route;


/* Auth::routes(); */
/* Autenticacion */
    /* Iniciar sesion */
    Route::get('/login',[LoginController::class,'showLoginForm'])->middleware('guest')->name('login');
    Route::post('/login',[LoginController::class,'login'])->middleware('guest'); 
    /* Cerrar sesion */
    Route::post('/logout',[LoginController::class,'logout'])->middleware('auth')->name('logout'); 
    /* Enviar correo recuperar contraseña */
    Route::get('/password/reset',[ForgotPasswordController::class,'showLinkRequestForm'])->name('password.request');
    Route::post('/password/email',[ForgotPasswordController::class,'sendResetLinkEmail'])->name('password.email');
    /* Restablecer contraseña */
    Route::get('/password/reset/{token}',[ResetPasswordController::class,'showResetForm'])->name('password.reset');
    Route::post('/password/reset',[ResetPasswordController::class,'reset'])->name('password.update');
/* Fin Autenticacion */

/* Rutas Protegidas Con Auth */
    Route::middleware(['auth'])->group(function(){
        Route::middleware(['password.changed'])->group(function(){
            /* Panel pricipal */
            Route::get('/',[DashboardController::class,'index'])->name('dashboard.index');
            /* Usuarios */
            Route::resource('/users',UserController::class)->except('show')->names('users');
            /* Dueños */
            Route::resource('/owners',OwnerController::class)->except('show')->names('owners');
            /* Criterios */
            Route::resource('/standards',StandardController::class)->names('standards');
            /* Aspectos */
            Route::resource('/aspects',AspectController::class)->only(['store','update','destroy'])->names('aspects');
        });
        /* datos del perfil */
        Route::get('/profile',[ProfileController::class,'index'])->name('profile.index');
        Route::put('/profile/password',[ProfileController::class,'changePassword'])->name('profile.password');
        Route::post('/profile/avatar',[ProfileController::class,'avatar'])->name('profile.avatar');
    });
/* Fin Rutas Protegidas Con Auth */

